
from django.utils.crypto import get_random_string
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models


# Create your models here.
class ShopItem(models.Model):
    name = models.CharField( max_length=255)
    description = models.CharField(max_length=2000)
    images = models.URLField(max_length=1000, default=None, null=True)
    price = models.IntegerField()
    
    #print will use this
    def __str__(self):
        return f'id:{self.id} name:{self.name} '

    class Meta:
        db_table = 'shop_item'


# Create your models here.
class Items(models.Model):

    shopItem = models.ForeignKey('shop.ShopItem', on_delete=models.CASCADE)
    name = models.CharField( max_length=255)
    price = models.IntegerField()
    images = models.URLField(max_length=1000, default=None, null=True)
    
    #print will use this
    def __str__(self):
        return f'id:{self.id} name:{self.name} '

    class Meta:
        db_table = 'items'

