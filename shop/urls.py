from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.ShopWelcome.as_view(), name='ShopWelcome'),
    
    path('shop/create-item/', views.ShopItemView.as_view(), name='v1_CreateShopItem'),    
    path('shop/create-variant/<int:shop_item_id>/', views.ShopVariantView.as_view(), name='v1_CreateShopItemVariant'), 
    path('shop/items/', views.ShopItemView.as_view(), name='v1_GetShopItems'), 
    path('shop/items/variants/<int:shop_item_id>/', views.ShopVariantView.as_view(), name='v1_GetShopItemVariants'),     
    # path('shop/items/', views.ShopItemView.as_view(), name='v1_News'),     
]
