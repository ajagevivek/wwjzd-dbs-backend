from user import models

from common.decorators import meta_data_response, session_authorize,\
    catch_exception
from common.exceptions import InvalidSerializerInputException

from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models

# from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db import transaction

from . import serializers

import logging
LOGGER = logging.getLogger(__name__)


class ShopWelcome(APIView):
    
    @catch_exception(LOGGER)
    @meta_data_response()
    def get(self, request, *args, **kwargs):
        return Response(
            {
                "message": "Welcome to the WWJZD DBS PROJECT API Server"
            },
            status=status.HTTP_200_OK)


class ShopItemView(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.ShopSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        shopItems = list(models.ShopItem.objects.all().values())
        return Response(shopItems, status=status.HTTP_200_OK)


class ShopVariantView(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.ItemSerializer(data=request.data)
        if serializer.is_valid():
            shopItemId = kwargs['shop_item_id']
            serializer.save(shopItemId)
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)


    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        shopItemId = kwargs['shop_item_id']
        shopItem = models.ShopItem.objects.get(id=shopItemId)
        items = list(models.Items.objects.filter(shopItem=shopItem).values())
        return Response(items, status=status.HTTP_200_OK)

