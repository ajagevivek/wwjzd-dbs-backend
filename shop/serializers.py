"""Serializers for the User App."""
import uuid
from . import models
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)



class ShopSerializer(serializers.Serializer):

    name = serializers.CharField(required=True)
    description = serializers.CharField(required=True)
    images = serializers.URLField(required=True)
    price = serializers.IntegerField(required=True)

    def save(self):
        # print(self.validated_data)
        new_shop = models.ShopItem.objects.create(**self.validated_data)
        new_shop.save()


class ItemSerializer(serializers.Serializer):

    name = serializers.CharField(required=True)
    images = serializers.URLField(required=True)
    price = serializers.IntegerField(required=True)

    shopItem = models.ForeignKey('shop.ShopItem', on_delete=models.CASCADE)

    def save(self,shop_item_id):
        shopItem = models.ShopItem.objects.get(id=shop_item_id)
        new_item = models.Items.objects.create(**self.validated_data,shopItem=shopItem)
        new_item.save()

