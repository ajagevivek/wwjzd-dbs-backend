from django.utils.crypto import get_random_string
from django.utils.crypto import get_random_string
from . import config
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models

# Create your models here.
class User(models.Model):
    email = models.EmailField(
        verbose_name='email address', max_length=255, null=True)
    full_name = models.CharField(verbose_name='full name', max_length=255)
    phone_number = models.CharField(max_length=255)
    profile_pic = models.CharField(max_length=1000, default=None, null=True)
    password_hash = models.CharField(max_length=1000)

    #print will use this
    def __str__(self):
        return f'id:{self.id} user_name:{self.full_name} email:{self.email}'


    def set_password(self, password):
        """Set the password for the User."""
        self.password_hash = encryption_utils.hash_password(password)
    
    @classmethod
    def get_by_id(cls, user_id):
        try:
            return cls.objects.get(id=user_id)
        except cls.DoesNotExist:
            raise NotAcceptableError('Invalid User ID')

    def check_password(self, password):
        """
        Validate the Password for this User.

        Check if the given string after hashing is same
        as the password_hash.
        """
        return encryption_utils.is_match(password, self.password_hash)
    class Meta:
        db_table = 'user'



class Login(models.Model):
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    session_token = models.CharField(
        editable=False, blank=True, null=True, max_length=64)
    updated_at = DateTimeField(auto_now=True)

    class Meta:
        db_table = 'user_login'

    def __unicode__(self):
        return "%s__%s" % (str(self.user), str(self.session_token))

    @classmethod
    def get_latest(cls, user_id):
        try:
            return cls.objects.filter(user_id=user_id).latest('updated_at')
        except cls.DoesNotExist:
            return None

    @classmethod
    def get_session_token(cls, user_id):
        return cls.get_latest(user_id).session_token

