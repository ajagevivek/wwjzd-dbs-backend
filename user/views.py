from user import models

from common.decorators import meta_data_response, session_authorize,\
    catch_exception
from common.exceptions import InvalidSerializerInputException

from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models

# from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db import transaction

from . import serializers

import logging
LOGGER = logging.getLogger(__name__)


class UserWelcome(APIView):
    
    @catch_exception(LOGGER)
    @meta_data_response()
    def get(self, request, *args, **kwargs):
        return Response(
            {
                "message": "Welcome to the WWJZD DBS PROJECT API Server"
            },
            status=status.HTTP_200_OK)


class EmailLogin(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    def post(self, request):
        serializer = serializers.UserLoginSerializer(data=request.data)
        if serializer.is_valid():
            session_data = serializer.save()
            return Response(session_data, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)


class UserRegistration(APIView):
    """View for the Registration API"""

    @catch_exception(LOGGER)
    @meta_data_response()
    def post(self, request):
        source = request.META.get("HTTP_SOURCE", "web")
        serializer = serializers.UserRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(source=source)
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)


# class UserActivate(APIView):

#     @catch_exception(LOGGER)
#     @meta_data_response()
#     def post(self, request, *args, **kwargs):
#         serializer = serializers.ActivateAccountSerializer(data={"token":request.GET['Token']})
#         if serializer.is_valid():
#             serializer.save()
#             return Response({}, status=status.HTTP_200_OK)
#         raise InvalidSerializerInputException(serializer.errors)


class Logout(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        models.Login.objects.\
            filter(user_id=auth_data.get('user_id')).delete()
        return Response({}, status=status.HTTP_200_OK)


# # class UserAccount(mixins.RetrieveModelMixin,
# #                   generics.GenericAPIView):
# #     queryset = models.Profile.objects.all()
# #     serializer_class = serializers.AccountSerializer
# #     lookup_field = 'user_id'

# #     @catch_exception(LOGGER)
# #     @meta_data_response()
# #     @session_authorize(user_id_key="user_id")
# #     def get(self, request, auth_data, *args, **kwargs):
# #         if auth_data.get('authorized'):
# #             return self.retrieve(request, *args, **kwargs)
# #         return Response({}, status.HTTP_401_UNAUTHORIZED)


class UserForgotPassword(APIView):
    """View for the Forgot Password API.
        will get email id from request
    """
    @catch_exception(LOGGER)
    @meta_data_response()
    def post(self, request):

        serializer = serializers.ForgotPasswordSerializer(data=request.data)
        
        if serializer.is_valid():
            return Response(serializer.save(),
                            status=status.HTTP_200_OK)

        raise InvalidSerializerInputException(serializer.errors)


# class UserResetPassword(APIView):

#     @catch_exception(LOGGER)
#     @meta_data_response()
#     def post(self, request):
#         serializer = serializers.ResetPasswordSerializer(data={**request.data, "token":request.GET["Token"]})
#         if serializer.is_valid():
#             message = serializer.save()
#             return Response(message, status=status.HTTP_200_OK)
#         raise InvalidSerializerInputException(serializer.errors)


# class UserPasswordChange(APIView):

#     @catch_exception(LOGGER)
#     @meta_data_response()
#     @session_authorize(user_id_key="user_id")
#     def post(self, request, auth_data, *args, **kwargs):
#         """Change Password API."""
#         serializer = serializers.PasswordChangeSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save(auth_data.get('user_id'))
#             return Response({}, status=status.HTTP_200_OK)
#         raise InvalidSerializerInputException(serializer.errors)


class UserProfile(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        user_id = auth_data.get('user_id')
        user = models.User.get_by_id(user_id)
        return Response({
            "full_name": user.full_name,
            "phone_number": user.phone_number,
            "email": user.email,
            "profile_pic": user.profile_pic,
        },
        status=status.HTTP_200_OK)

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.ProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(auth_data.get('user_id'))
            user_id = auth_data.get('user_id')
            user = models.User.get_by_id(user_id)
            return Response({
                "full_name": user.full_name,
                "phone_number": user.phone_number,
                "email": user.email,
                "profile_pic": user.profile_pic,
            }, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)
