from user import models
from django.conf import settings
import logging
logger = logging.getLogger(__name__)


class Authentication(object):

    def __init__(self, authentication_data):
        self.session_token = authentication_data.get("session_token")
        self.user_id = authentication_data.get("user_id")

    def _login(self):
        # LOGGER.info('in login')
        # LOGGER.info(self.user_id)
        # LOGGER.info(self.session_token)
        if not self.user_id:
            return models.Login.objects.filter(
                session_token=self.session_token).first()
        return models.Login.objects.filter(
            user_id=self.user_id,
            session_token=self.session_token).first()

    def validated_data(self):
        
        if self.session_token == settings.MASTER_SESSION_TOKEN:
            return True, self.user_id
        login_object = self._login()
        if login_object:
            # update the updated at time of the login object
            login_object.save()
            return True, login_object.user_id
        return False, None
