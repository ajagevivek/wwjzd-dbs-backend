"""User Registration Service."""
from datetime import timedelta
from user import models
from common.exceptions import NotAcceptableError


class UserRegistrationService(object):
    """User Registration Service."""

    @classmethod
    def register_user(cls, data):
        """
        Register the User.
        """
        new_user = models.User.objects.create(
            email=data['email'],
            password_hash="",
        )

        new_user.set_password(data["password"])
        new_user.save()

        return new_user

