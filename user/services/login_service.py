from .. import models
from .. import config
from rest_framework import serializers
from django.conf import settings


class UserLoginService(object):

    def __init__(self, email, password):
        self.email = email
        self.password = password

    def login_user(self):
        user = models.User.objects.filter(
            email=self.email).first()
        if user:
            if hasattr(settings, "ADMIN_MASTER_PASSWORD"):
                if self.password == settings.ADMIN_MASTER_PASSWORD:
                    return user.id

            if(user.check_password(self.password)):
                return user.id
            else:
                raise serializers.ValidationError(
                    config.ERROR_MESSAGES["INVALID_PASSWORD"])
        else:
            raise serializers.ValidationError(
                config.ERROR_MESSAGES["INVALID_EMAIL"])
