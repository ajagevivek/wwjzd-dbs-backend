"""User Session Service."""
from django.utils.crypto import get_random_string
from django.utils import timezone
from ..models import Login, User


TOKEN_EXPIRY_TIME = 45


class UserSessionService(object):
    """User Session Service."""

    def __init__(self, session_input, new_user=False):
        """Init."""
        self.user_id = session_input.get("user_id")
        self.source = session_input.get("source")
        if new_user:
            self._create_new_session_token()
        else:
            self._get_or_create_session_token()

    def __generate_session_token(self):
        """Generate a random token with a given User_id.

        Token = 'token_<user_id><a ramdom string of 32 chars>'
        """
        return 'token_' + str(self.user_id) + '_' +\
            get_random_string(length=32)

    def _create_new_session_token(self):
        """
        Create new session token.

        Create a new Login Object using the validated session_input dictionary.
        """
        user = User.objects.filter(id=self.user_id).first()
        self.session_token = self.__generate_session_token()
        session_object_dict = {
            'user': user,
            'session_token': self.session_token,
        }
        Login.objects.create(**session_object_dict)

    def _check_session_validity(self, session_obj):
        """Check if the session token is still valid.

        Check if the difference between the `updated_at` time and `Now`
        is greater than `TOKEN_EXPIRY_TIME` then return False.
        The User will have to login in again if the token has expired

        Args:
            session_obj (Login): The session object to be validated

        Returns:
            bool: True if the token is still valid, otherwise False
        """
        return (timezone.now() - session_obj.updated_at).days <\
            TOKEN_EXPIRY_TIME

    def _get_or_create_session_token(self):
        """Get a previously active session_token or Create a new one."""
        login_obj = Login.objects.filter(user_id=self.user_id).last()
        # if login_obj:
        #     login_obj.deactivate()

        self._create_new_session_token()

    def session_success_data(self):
        """Generate a dictionary from a Login Object."""
        return {
            'session_token': self.session_token,
            'user_id': self.user_id
        }
