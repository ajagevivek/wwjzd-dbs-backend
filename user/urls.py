from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.UserWelcome.as_view(), name='UserWelcome'),
    
    path('user/register/', views.UserRegistration.as_view(), name='v1_UserRegistration'),    

    # path('user/activate/', views.UserActivate.as_view(), name='v1_UserActivate'),


    # # Login APIs
    path('user/login/', views.EmailLogin.as_view(), name='v1_EmailLogin'),

    path('user/forgot_password/', views.UserForgotPassword.as_view(), name='v1_UserForgotPassword'),

    # path('user/reset_password/', views.UserResetPassword.as_view(), name='v1_UserResetPassword'),

    path('user/<int:user_id>/logout/', views.Logout.as_view(), name='v1_emaillogout'),

    # # Profile Page APIs
    path('user/<int:user_id>/profile/', views.UserProfile.as_view(), name='v1_UserProfile'),

    # path('user/<int:user_id>/password/', views.UserPasswordChange.as_view(), name='v1_UserPassword'),

]
