"""Serializers for the User App."""
import uuid
from . import models
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from . import config
from .services import authentication_service
from .services.registration_service import UserRegistrationService
from .services.login_service import UserLoginService
from .services.session_service import UserSessionService
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)


class AuthenticationSerializer(serializers.Serializer):
    session_token = serializers.CharField(max_length=64)
    user_id = serializers.IntegerField()

    def verify_and_update_session(self):
        authentication = authentication_service.Authentication(
            self.validated_data)
        return authentication.validated_data()[0]


class UserRegistrationSerializer(serializers.Serializer):
    """Custom Serializer to validate and create a new User."""

    email = serializers.EmailField(required=True, validators=[
        UniqueValidator(
            message=config.ERROR_MESSAGES["EMAIL_ALREADY_EXISTS"],
            queryset=models.User.objects.all())])
    password = serializers.CharField(
        min_length=6, max_length=60, required=True)

    def save(self, **kwargs):
        """Create a Login Session after saving the new User."""
        
        new_user = UserRegistrationService.register_user(self.validated_data)
        

# class ActivateAccountSerializer(serializers.Serializer):
#     token = serializers.CharField()

#     def save(self):
#         token = None
#         try:
#             token = models.Token.active_objects.get(code=self.validated_data["token"],
#                                                 category="REGISTRATION_CODE")
#         except:
#             raise NotAcceptableError('Invalid Activation Token')

#         user = token.user
#         # print(user.json())
#         user.is_activated = True
#         # print(user.json())
#         user.save()
#         # print(user.json())
#         token.save()
#         token.deactivate()
#         return token.json()


class UserLoginSerializer(serializers.Serializer):
    """Custom Serializer to validate and create a Login Session."""
    email = serializers.EmailField(required=True)
    password = serializers.CharField(
        min_length=6, max_length=60, required=True)

    def validate(self, data):
        """Validate user email and password."""
        user = None
        try:
            user = models.User.objects.get(email=data["email"])
        except models.User.DoesNotExist:
            raise NotAcceptableError('Invalid Email')

        self.user_id = UserLoginService(email=data["email"],
                                        password=data["password"]).login_user()
        return data

    def save(self, **kwargs):
        """Create a new login session bbject or return an old one."""
        # print "Entering Login"

        session_input = {
            "source": kwargs.get("source"),
            "user_id": self.user_id
        }
        return UserSessionService(session_input).session_success_data()


class ForgotPasswordSerializer(serializers.Serializer):
    """Custom Serializer to validate and create a Login Session."""
    email = serializers.EmailField(required=True)

    def save(self, **kwargs):
        """Send a reset link via email"""
        # Check if its an valid email
        try:
            user = models.User.objects.get(email=self.validated_data['email'])
        except models.User.DoesNotExist:
            raise NotAcceptableError('Invalid Email')
        except models.User.MultipleObjectsReturned:
            raise NotAcceptableError('Integrity Error')

        # send reset password email


# class ResetPasswordSerializer(serializers.Serializer):

#     token = serializers.CharField()
#     password = serializers.CharField(min_length=6, max_length=60)

#     def save(self):
#         token = models.Token.active_objects.filter(
#             code=self.validated_data["token"],
#             category="PASSWORD_RECOVERY",
#             created_at__gte=timezone.now() - timedelta(days=1)).first()
#         if token:
#             token.user.password_hash = encryption_utils.hash_password(self.validated_data['password'])
#             token.user.save()
#             token.save()
#             token.deactivate()
#         else:
#             raise NotAcceptableError('Invalid Reset Token')


class ProfileSerializer(serializers.Serializer):
    profile_pic = serializers.CharField(required=False)
    full_name = serializers.CharField(required=False)
    phone_number = serializers.CharField(required=False)

    def save(self, user_id):
        user = models.User.get_by_id(user_id)

        def set(field):
            setattr(user, field, self.validated_data.get(field) or getattr(user, field))

        for key in self.validated_data:
            set(key)

        user.save()


# class PasswordChangeSerializer(serializers.Serializer):

#     old_password = serializers.CharField()
#     new_password = serializers.CharField(min_length=6, max_length=60)

#     def save(self, user_id):
#         # check if the old pass word is same or not
#         user = models.User.get_by_id(user_id)
#         if not user.check_password(self.validated_data['old_password']):
#             raise NotAcceptableError('Incorrect Password')

#         user.set_password(self.validated_data['new_password'])
#         user.save()


