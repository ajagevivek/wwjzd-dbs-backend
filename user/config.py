ERROR_MESSAGES = {

    "EMAIL_ALREADY_EXISTS":
        "A user with this email already exists",
    "INVALID_CREDENTIALS":
        "The email or password is incorrect",
    "INVALID_EMAIL":
        "This email is not associated with any Account",
    "INVALID_PASSWORD":
        "The email/password is incorrect",
    "INVALID_IDENTIFIER":
        "Your email or phone is not associated with any Account",
    "INVALID_OTP":
        "Invalid OTP",
    "EMAIL_REGISTERED":
        "There is a registered user with following name: {user_name}."
        " Please login if it is your account or create new account"
        " with different email address.",
}


REFERRAL_CODE_LENGTH = 6
