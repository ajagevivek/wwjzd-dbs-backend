"""Serializers for the User App."""
import uuid
from . import models
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)


#Contact Serilizer
class ContactSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    subject = serializers.CharField(required=True)
    message = serializers.CharField(required=True)

    def save(self):
        # print(self.validated_data)
        new_contact = models.Contact.objects.create(email=self.validated_data['email'],message=self.validated_data['message'],subject=self.validated_data['subject'],name=self.validated_data['name'],)

        # new_user.set_password(data["password"])
        # new_user.save()
        new_contact.save()

