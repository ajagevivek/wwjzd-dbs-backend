from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.ContactWelcome.as_view(), name='ContactWelcome'),
    
    #Contact Us API
    path('contact-us/', views.ContactUs.as_view(), name='v1_ContactUs'),    

   

]
