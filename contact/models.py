
from django.utils.crypto import get_random_string
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models


# Contact Model
class Contact(models.Model):
    email = models.EmailField(
        verbose_name='email address', max_length=255, null=True)
    name = models.CharField(verbose_name='full name', max_length=255)
    subject = models.CharField(max_length=255)
    message = models.CharField(max_length=1000, default=None, null=True)

    #print will use this
    def __str__(self):
        return f'id:{self.id} user_name:{self.name} email:{self.email}'

    class Meta:
        db_table = 'contact'

