from functools import wraps
from django.db import IntegrityError
from django.http import Http404
from django.conf import settings

from rest_framework.response import Response
from rest_framework import status, serializers


from . responses import MetaDataResponse
from . exceptions import NotAcceptableError

from user.services import authentication_service

import logging
logger = logging.getLogger(__name__)


#Session Authoizer Decorator
def session_authorize(user_id_key='pk', *args, **kwargs):
    def deco(f):
        
        #FN to abstract user_id
        def abstract_user_id(request, **kwargs):
            user_id = None
            if user_id_key in kwargs:
                user_id = kwargs[user_id_key]
            elif request.method == 'GET':
                try:
                    user_id = request.query_params.get(user_id_key)[0]
                except TypeError:
                    user_id = None
            else:
                user_id = request.data.get(user_id_key)
            return int(user_id) if user_id else None

        #FN to abstract session_token
        def abstract_session_token(request, **kwargs):
            session_token_header_key = 'HTTP_SESSION_TOKEN'
            session_token = request.META.get(session_token_header_key)
            if session_token:
                return session_token
            
            session_token_key = 'session_token'
            if session_token_key in kwargs:
                session_token = kwargs[session_token_key]
            else:
                try:
                    session_token = request.query_params.get(
                        session_token_key)
                except TypeError:
                    session_token = None

            return session_token

        #FN to validated session_token
        @wraps(f)
        def decorated_function(*args, **kwargs):
            request = args[1]
           
            auth_data = {
                'user_id': abstract_user_id(request, **kwargs),
                'session_token': abstract_session_token(request)
            }


            auth_data['authorized'],auth_data['user_id'] = authentication_service.Authentication(auth_data).validated_data()
            # print('autho -')
            # print(auth_data['authorized'])
            if not auth_data['authorized']:
                # return unauthorised status
                return Response({}, status.HTTP_401_UNAUTHORIZED)

            return f(auth_data=auth_data, *args, **kwargs)

        return decorated_function
    return deco


def default_logger():
    logger = logging.getLogger("From the Decorator file")
    return logger


#Catch Decorator
def catch_exception(logger=default_logger()):
    def deco(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            try:
                return f(*args, **kwargs)

            except IntegrityError as e:
                logger.exception("IntegrityError:%s" % str(e))
                return MetaDataResponse(
                    error={
                        'type': 'IntegrityError',
                        'message': str(e),
                        'code': status.HTTP_400_BAD_REQUEST
                    }
                )

            except Http404 as e:
                logger.exception(str(e))
                return MetaDataResponse(
                    error={
                        'type': 'ObjectNotFoundError',
                        'message': str(e),
                        'code': status.HTTP_400_BAD_REQUEST
                    }
                )

            except AssertionError as e:
                logger.exception(str(e))
                return MetaDataResponse(
                    error={
                        'type': 'AssertionError',
                        'message': str(e),
                        'code': status.HTTP_400_BAD_REQUEST
                    }
                )

            except Exception as e:
                if hasattr(e, 'get_json'):
                    logger.exception(f"Encountered Exception {e.get_json()}")
                    return MetaDataResponse(
                        error=e.get_json()
                    )
                logger.exception(str(e))
                return MetaDataResponse(
                    error={
                        'type': 'InternalServerException',
                        'message': str(e),
                        'code': status.HTTP_500_INTERNAL_SERVER_ERROR
                    },
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR
                )
        return decorated_function
    return deco


#Response Formatter
def meta_data_response(meta=""):
    def deco(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            vanilla_response = f(*args, **kwargs)
            return MetaDataResponse(
                vanilla_response.data,
                meta, status=vanilla_response.status_code)
        return decorated_function
    return deco
