from rest_framework import status
from .utils.error_wrapper import error_wrapper


#Error Standardization
class ErrorMessage(Exception):

    def __init__(self, msg):
        self.value = msg

    def __str__(self):
        return repr(self.value)


#Base Exception Class

class BaseException(Exception):

    def __dict__(self):
        return self.get_json()
    
    def __repr__(self):
        return self.get_json()
    
    def get_json(self):
        return {}


#Serializer Error Formatter
class InvalidSerializerInputException(BaseException):
    
    def __init__(self, errors):
        self.response = ', '.join(error_wrapper(errors))
        self.status = 400
        self.meta = 'InvalidInputException'
        super().__init__()

    def __str__(self):
        return ("Response: {response}, Meta: {meta}, Status: {status}".format(response=self.response, meta=self.meta, status=self.status))

    def get_json(self):
        return {
            "message": self.response,
            "type": self.meta,
            "code": self.status
        }



#Not acceptable Error
class NotAcceptableError(BaseException):

    def __init__(self, response, meta=None, status=400):
        self.response = response
        self.status = status
        self.meta = meta if meta else self.__class__.__name__
        super().__init__()

    def __str__(self):
        return ("Response: {response}, Meta: {meta}, Status: {status}".format(response=self.response, meta=self.meta, status=self.status))

    def get_json(self):
        return {
            "message": self.response,
            "type": self.meta,
            "code": self.status
        }


