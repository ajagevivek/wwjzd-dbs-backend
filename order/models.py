
from django.utils.crypto import get_random_string
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models


# Create your models here.
class Order(models.Model):

    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    address = models.ForeignKey('order.DeliveryAddress',on_delete=SET_NULL,null=True)
    orderItems=models.CharField(max_length=255)
    
    
    #print will use this
    def __str__(self):
        return f'id:{self.id} name:{self.grandTotal} '

    class Meta:
        db_table = 'order'


# Create your models here.
class DeliveryAddress(models.Model):

    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    contactNumber = models.IntegerField()
    pinCode = models.IntegerField()
    state = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    address = models.CharField(max_length=1000)
    
    #print will use this
    def __str__(self):
        return f'id:{self.id} name:{self.name} '

    class Meta:
        db_table = 'delivery_address'

