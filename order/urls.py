from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.OrderViewWelcome.as_view(), name='OrderViewWelcome'),
    
    path('order/calculate/', views.OrderView.as_view(), name='v1_CalculateOrder'),    
    path('checkout/<int:user_id>/', views.CheckoutView.as_view(), name='v1_CalculateOrder'),    
    path('delivery-address/<int:user_id>/', views.DeliveryAddressView.as_view(), name='v1_CalculateOrder'),    
    # path('shop/items/', views.ShopItemView.as_view(), name='v1_News'),     
]
