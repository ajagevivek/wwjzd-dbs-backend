"""Serializers for the User App."""
import uuid
from . import models
from user.models import User
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)



class AddressSerializer(serializers.Serializer):

    name = serializers.CharField(required=True)
    contactNumber = serializers.IntegerField(required=False)
    pinCode = serializers.IntegerField(required=True)
    state =serializers.CharField(required=True)
    city = serializers.CharField(required=True)
    address = serializers.CharField(required=True)


    def save(self, user_id):
        user = User.get_by_id(user_id)
        new_address = models.DeliveryAddress.objects.create(**self.validated_data,user=user)
        new_address.save()


