from user import models

from common.decorators import meta_data_response, session_authorize,\
    catch_exception
from common.exceptions import InvalidSerializerInputException
from common.exceptions import NotAcceptableError

from user.models import User
from shop.models import Items
from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models

# from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db import transaction

from . import serializers

import logging
LOGGER = logging.getLogger(__name__)


class OrderViewWelcome(APIView):
    
    @catch_exception(LOGGER)
    @meta_data_response()
    def get(self, request, *args, **kwargs):
        return Response(
            {
                "message": "Welcome to the WWJZD DBS PROJECT API Server"
            },
            status=status.HTTP_200_OK)


class DeliveryAddressView(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.AddressSerializer(data=request.data)
        if serializer.is_valid():
            userId = kwargs['user_id']
            serializer.save(userId)
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        userId = kwargs['user_id']
        userToFilter = User.get_by_id(userId)
        addressItems = list(models.DeliveryAddress.objects.filter(user=userToFilter).values())
        return Response(addressItems, status=status.HTTP_200_OK)


class OrderView(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        try:
            itemsId = request.data['item_id']
            items = itemsId.split(',')
            totalPrice=0
            for singleItem in items:
                if singleItem:
                    shopItem = Items.objects.get(id=singleItem)
                    totalPrice = shopItem.price + totalPrice
            return Response({'subTotal':totalPrice,'discountTotal':0,'shippingTotal':len(items)*10,'grandTotal':totalPrice+len(items)*10}, status=status.HTTP_200_OK)
        except:
            raise NotAcceptableError('Invalid Input')

   
class CheckoutView(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        address = None
        try:
            address = models.DeliveryAddress.objects.get(id=request.data['address_id'])
        except:
            raise NotAcceptableError('Invalid Request')
        user = User.get_by_id(kwargs['user_id'])
        itemsId = request.data['item_id']
        if address and itemsId and user:
            new_checkout = models.Order.objects.create(user=user,address=address,orderItems=itemsId)
            new_checkout.save()
            return Response({}, status=status.HTTP_200_OK)
        else:
            raise NotAcceptableError('Invalid Request')


    
