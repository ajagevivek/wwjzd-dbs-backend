"""Serializers for the User App."""
import uuid
from . import models
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)


#Gallery Serializer
class GallerySerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    url = serializers.URLField(required=True)
    categoryName =  serializers.CharField(required=True)

    def save(self):
        # print(self.validated_data)
        categoryToAdd = models.Category.objects.filter(name=self.validated_data['categoryName']).first()
        if categoryToAdd:
            new_gallery = models.Gallery.objects.create(category=categoryToAdd, name=self.validated_data['name'],url=self.validated_data['url'],categoryName=self.validated_data['categoryName'],)
            new_gallery.save()
        else:
            raise NotAcceptableError('Invalid Catergory Name')

#Category Serializer

class CategorySerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    url = serializers.URLField(required=True)

    def save(self):
        # print(self.validated_data)
        new_category = models.Category.objects.create(name=self.validated_data['name'],url=self.validated_data['url'])

        # new_user.set_password(data["password"])
        # new_user.save()
        new_category.save()

