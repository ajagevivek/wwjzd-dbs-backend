from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.GalleryWelcome.as_view(), name='ContactWelcome'),
    
    #API to Create Gallery Item
    path('gallery/', views.GalleryView.as_view(), name='v1_Gallery'),    
    
    #API to Fetch Gallery based on Catergory
    path('gallery/<int:category_id>', views.GalleryView.as_view(), name='v1_Gallery'),    

    #API to Fetch Categories and Create Category
    path('category/', views.CategoryView.as_view(), name='v1_Category'),    

]
