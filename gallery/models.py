
from django.utils.crypto import get_random_string
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models



# Catergory Model
class Category(models.Model):
    url = models.URLField(
         max_length=100, null=True)
    name = models.CharField(verbose_name='category url name', max_length=255)

    #print will use this
    def __str__(self):
        return f'id:{self.id} catergory url :{self.url} catergory name :{self.name}'

    class Meta:
        db_table = 'category'
    
    @classmethod
    def get_by_id(cls, category_id):
        try:
            return cls.objects.get(id=category_id)
        except cls.DoesNotExist:
            raise NotAcceptableError('Invalid Category ID')



# Gallery Model
class Gallery(models.Model):
    category = models.ForeignKey('gallery.Category', on_delete=models.CASCADE)
    url = models.URLField(
         max_length=100, null=True)
    name = models.CharField(verbose_name='image url name', max_length=255)
    categoryName = models.CharField(verbose_name='category name', max_length=255)

    #print will use this
    def __str__(self):
        return f'id:{self.id} url:{self.url} categoryName:{self.categoryName}'

    class Meta:
        db_table = 'gallery'



