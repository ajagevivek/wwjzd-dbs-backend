"""Serializers for the User App."""
import uuid
from . import models
from user.models import User
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)


#Bread API Serializer
class BreadSerializer(serializers.Serializer):
    tag = serializers.CharField(required=True)
    spotifyUrl = serializers.URLField(required=False)
    quote = serializers.CharField(required=True)
    day = serializers.CharField(required=True)
    month = serializers.CharField(required=True)
    heading = serializers.CharField(required=True)
    description = serializers.CharField(required=True)
    songAlbum = serializers.CharField(required=True)
    songName = serializers.CharField(required=True)
    
    #Fn to save Bread Once Validated
    def save(self , *args, **kwargs):
        # print(self.validated_data)
        new_bread = models.Bread.objects.create(**self.validated_data)
        new_bread.save()


