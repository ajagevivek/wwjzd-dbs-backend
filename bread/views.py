from user.models import User

from common.decorators import meta_data_response, session_authorize,\
    catch_exception
from common.exceptions import InvalidSerializerInputException
from common.exceptions import NotAcceptableError
from django.forms.models import model_to_dict

from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models

# from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db import transaction

from . import serializers

import logging
LOGGER = logging.getLogger(__name__)


class BreadWelcome(APIView):
    
    @catch_exception(LOGGER)
    @meta_data_response()
    def get(self, request, *args, **kwargs):
        return Response(
            {
                "message": "Welcome to the WWJZD DBS PROJECT API Server"
            },
            status=status.HTTP_200_OK)


#Tag view 
class TagView(APIView):

    #Tag GET API 
    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        allTags = list(models.Bread.objects.values('tag').distinct().values('tag'))
        return Response(allTags, status=status.HTTP_200_OK)


#Bread view 
class BreadView(APIView):

    #Bread Create API
    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.BreadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)
    
    #Bread Fetch API
    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        month = request.GET.get('month', None)
        day = request.GET.get('day', None)
        tag = request.GET.get('tag', None)
        allBreads = models.Bread.objects
        if month:
            allBreads = allBreads.filter(month=month)
        if day:
            allBreads = allBreads.filter(day=day)
        if tag:
            allBreads = allBreads.filter(tag=tag)

        allBreadData = list(allBreads.values())
        return Response(allBreadData, status=status.HTTP_200_OK)
    
    
class BreadActionsView(APIView):

    #Bread Fav Fetch API
    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):

        userId = kwargs['user_id']
        userToModify = User.get_by_id(userId)
        listBreadsLiked = list(models.BreadLiked.objects.filter(user=userToModify).values('bread_id'))
        breadsToSend = []
        for singleBread in listBreadsLiked:
            breadId = singleBread['bread_id']
            breadsToSend.append(model_to_dict(models.Bread.objects.get(id=breadId)))
        return Response(breadsToSend, status=status.HTTP_200_OK)

    #Bread Fav Post API
    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        breadId = kwargs['bread_id']
        breadToModify = models.Bread.objects.get(id=breadId)
        userId = kwargs['user_id']
        userToModify = User.get_by_id(userId)
        try:
            checkIfAlreadyLiked = models.BreadLiked.objects.get(user=userToModify,bread=breadToModify)
            checkIfAlreadyLiked.delete()
            return Response({}, status=status.HTTP_200_OK)
        except:
            models.BreadLiked.objects.create(user=userToModify,bread=breadToModify)
            return Response({}, status=status.HTTP_200_OK)
            
    #Bread Delete API
    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def delete(self, request, auth_data, *args, **kwargs):
        breadId = kwargs['bread_id']
        breadToModify = models.Bread.objects.get(id=breadId)
        if breadToModify:
            breadToModify.delete()
            return Response({}, status=status.HTTP_200_OK)
        else:
            raise NotAcceptableError('Bread not found')

        
    

