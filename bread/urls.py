from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.BreadWelcome.as_view(), name='BreadWelcome'),
    
    #Creat Bread API
    path('bread/create/', views.BreadView.as_view(), name='v1_reminderCreate'),
    #GET Bread API
    path('bread', views.BreadView.as_view(), name='v1_reminderGet'),
    #GET Tags API
    path('tags', views.TagView.as_view(), name='v1_reminderGet'),
    #Bread Action API
    path('bread/actions/<int:bread_id>/<int:user_id>/', views.BreadActionsView.as_view(), name='v1_reminderGet'),
    #FAV bread API
    path('bread/favourites/<int:user_id>/', views.BreadActionsView.as_view(), name='v1_reminderGet'),

]
