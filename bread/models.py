from django.utils.crypto import get_random_string
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models


# Bread Model
class Bread(models.Model):
    
    tag = models.CharField( max_length=255)
    spotifyUrl = models.URLField( max_length=255)
    quote = models.CharField( max_length=255)
    day = models.CharField( max_length=255)
    month = models.CharField( max_length=255)
    heading = models.CharField( max_length=255)
    description = models.CharField( max_length=2000)
    songAlbum = models.CharField( max_length=255)
    songName = models.CharField( max_length=255)
    

    #print will use this
    def __str__(self):
        return f'time:{self.heading}'

    class Meta:
        db_table = 'bread'
    

    #Get Bread by ID
    @classmethod
    def get_by_id(cls, bread_id):
        try:
            return cls.objects.get(id=bread_id)
        except cls.DoesNotExist:
            raise NotAcceptableError('Invalid Bread ID')

# Bread Action Model
class BreadLiked(models.Model):
    
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    bread = models.ForeignKey('bread.Bread', on_delete=models.CASCADE)
    

    #print will use this
    def __str__(self):
        return f'time:{self.id}'

    class Meta:
        db_table = 'breadliked'
    

