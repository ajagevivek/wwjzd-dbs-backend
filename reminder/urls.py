from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.ReminderWelcome.as_view(), name='ReminderWelcome'),
    
    path('reminder/<int:user_id>/create/', views.ReminderView.as_view(), name='v1_reminderCreate'),
    path('reminder/<int:user_id>/<int:reminder_id>/edit/', views.ReminderEdit.as_view(), name='v1_reminderEdit'),
    path('reminder/<int:user_id>/<int:reminder_id>/delete/', views.ReminderDelete.as_view(), name='v1_reminderDelete'),
    path('reminder/<int:user_id>', views.ReminderAll.as_view(), name='v1_reminderGetAll'),

]
