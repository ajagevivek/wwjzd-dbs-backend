
from django.utils.crypto import get_random_string
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models


# Create your models here.
class Reminder(models.Model):
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    time = models.CharField( max_length=255)

    #print will use this
    def __str__(self):
        return f'time:{self.time}'

    class Meta:
        db_table = 'reminder'
    
    @classmethod
    def get_by_id(cls, reminder_id):
        try:
            return cls.objects.get(id=reminder_id)
        except cls.DoesNotExist:
            raise NotAcceptableError('Invalid Reminder ID')

