"""Serializers for the User App."""
import uuid
from . import models
from user.models import User
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)



class ReminderSerializer(serializers.Serializer):
    time = serializers.CharField(required=True)

    def save(self , *args, **kwargs):
        # print(self.validated_data)
        user_id = kwargs['user_id']
        userToAddReminder = User.get_by_id(user_id)
        if userToAddReminder:
            new_reminder = models.Reminder.objects.create(user=userToAddReminder,time=self.validated_data['time'])
            new_reminder.save()


class ReminderEditSerializer(serializers.Serializer):
    time = serializers.CharField(required=True)

    def save(self , *args, **kwargs):
        # print(self.validated_data)
        user_id = kwargs['user_id']
        reminder_id = kwargs['reminder_id']
        userToAddReminder = User.get_by_id(user_id)
        reminderToAdd = models.Reminder.get_by_id(reminder_id)

        if reminderToAdd.user == userToAddReminder:
            reminderToAdd.time = self.validated_data['time']
            reminderToAdd.save()
        else:
            NotAcceptableError('Reminder Not Associated with the user')

