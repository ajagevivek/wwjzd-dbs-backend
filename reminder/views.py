from user.models import User

from common.decorators import meta_data_response, session_authorize,\
    catch_exception
from common.exceptions import InvalidSerializerInputException
from common.exceptions import NotAcceptableError

from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models

# from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db import transaction

from . import serializers

import logging
LOGGER = logging.getLogger(__name__)


class ReminderWelcome(APIView):
    
    @catch_exception(LOGGER)
    @meta_data_response()
    def get(self, request, *args, **kwargs):
        return Response(
            {
                "message": "Welcome to the WWJZD DBS PROJECT API Server"
            },
            status=status.HTTP_200_OK)


class ReminderView(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.ReminderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user_id=kwargs['user_id'])
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)


class ReminderEdit(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.ReminderEditSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user_id=kwargs['user_id'],reminder_id=kwargs['reminder_id'])
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)

class ReminderDelete(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):

        userId = kwargs['user_id']
        reminderId = kwargs['reminder_id']
        userToFind = User.get_by_id(userId)
        reminderToDelete = models.Reminder.objects.get(id=reminderId)
        if reminderToDelete.user==userToFind:
            reminderToDelete.delete()
            return Response({}, status=status.HTTP_200_OK)
        else:
             raise NotAcceptableError('Reminder Not Found')

        


class ReminderAll(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        userId = kwargs['user_id']
        userToFind = User.get_by_id(userId)
        allReminders = list(models.Reminder.objects.filter(user=userToFind).values('id','time'))
        return Response(allReminders, status=status.HTTP_200_OK)
