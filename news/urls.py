from django.urls import path

from . import views

urlpatterns = [
    # Registration APIs
    path('', views.NewsWelcome.as_view(), name='NewsWelcome'),
    
    # News Get and Create API

    path('news/', views.NewsView.as_view(), name='v1_News'),    

   

]
