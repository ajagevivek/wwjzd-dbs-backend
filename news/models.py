
from django.utils.crypto import get_random_string
import string
from common.utils import encryption_utils
from common.exceptions import NotAcceptableError

from django.db.models import *
from django.db import models


# News Model
class News(models.Model):
    title = models.CharField( max_length=255)
    content = models.CharField(max_length=2000)
    urlToImage = models.URLField(max_length=1000, default=None, null=True)
    url = models.URLField(max_length=1000, default=None, null=True)
    #print will use this
    def __str__(self):
        return f'id:{self.id} title:{self.title} '

    class Meta:
        db_table = 'news'

