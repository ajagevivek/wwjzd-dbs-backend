"""Serializers for the User App."""
import uuid
from . import models
from rest_framework import serializers

from common.exceptions import NotAcceptableError
from rest_framework.validators import UniqueValidator
from common.utils import encryption_utils
from django.utils import timezone
from datetime import timedelta
from django.conf import settings

import logging
logger = logging.getLogger(__name__)


# News Serilizer

class NewsSerializer(serializers.Serializer):
    title = serializers.CharField(required=True)
    content = serializers.CharField(required=True)
    urlToImage = serializers.URLField(required=True)
    url = serializers.URLField(required=True)

    def save(self):
        # print(self.validated_data)
        new_news = models.News.objects.create(**self.validated_data)
        new_news.save()

