from user import models

from common.decorators import meta_data_response, session_authorize,\
    catch_exception
from common.exceptions import InvalidSerializerInputException

from rest_framework import generics, mixins, status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models

# from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db import transaction

from . import serializers

import logging
LOGGER = logging.getLogger(__name__)


class NewsWelcome(APIView):
    
    @catch_exception(LOGGER)
    @meta_data_response()
    def get(self, request, *args, **kwargs):
        return Response(
            {
                "message": "Welcome to the WWJZD DBS PROJECT API Server"
            },
            status=status.HTTP_200_OK)


class NewsView(APIView):

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def post(self, request, auth_data, *args, **kwargs):
        serializer = serializers.NewsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({}, status=status.HTTP_200_OK)
        raise InvalidSerializerInputException(serializer.errors)

    @catch_exception(LOGGER)
    @meta_data_response()
    @session_authorize(user_id_key="user_id")
    def get(self, request, auth_data, *args, **kwargs):
        newsToSend = list(models.News.objects.all().values())

        return Response(newsToSend, status=status.HTTP_200_OK)
